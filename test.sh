#!/bin/bash

# This scripts does a simple comparison of two files
# using diff cmd

# Variable to check for errors
error=0

echo "Run code"
gcc  test.c -o test -l pthread -lm #> actual_result1.txt
./test
diff -l result1.txt resultmatrix.txt > diff.txt
diff -l result2.txt resultpi.txt > diff.txt
echo "resultado retorno matriz y pi: " 
cat result1.txt
cat result2.txt
echo -e "\nShow differences pi y matriz"
cat diff.txt
echo "Si retorno pi y matriz es 00 no hay diferencias"

rm diff.txt
rm result1.txt
rm result2.txt

if(($error == 1));then
  exit -1
fi

